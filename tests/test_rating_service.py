import pytest 
from rating.rating import (
        get_average_rating,
        add_rating_to_average,
        rating_db 
)

def test_get_average_rating_no_product_id():
    rating_db.clear()
    product_id = 0

    dt = get_average_rating(product_id=0)
    assert(dt["averageRating"] is None)

def test_get_average_rating_has_product_id():
    rating_db.clear()
    add_rating_to_average(0, 4)
    dt = get_average_rating(product_id=0)
    assert(dt["averageRating"] == 4)

def test_calculate_average_correct():
    rating_db.clear()
    add_rating_to_average(0, 4)
    add_rating_to_average(0, 1)
    dt = get_average_rating(product_id=0)
    # we round to 1 decimal
    assert(dt["averageRating"] == 2.5)

def test_calculate_average_rounding():
    rating_db.clear()
    add_rating_to_average(0, 4)
    add_rating_to_average(0, 1)
    dt = get_average_rating(product_id=0)
    # we round to 1 decimal
    assert(dt["averageRating"] == 2.5)

def test_negative_ratings():
    rating_db.clear()
    add_rating_to_average(0, -2)
    add_rating_to_average(0, 2)
    dt = get_average_rating(product_id=0)
    assert(dt["averageRating"] == 0)

def test_zero_ratings():
    rating_db.clear()
    add_rating_to_average(0, 0)
    add_rating_to_average(0, 0)
    dt = get_average_rating(product_id=0)
    assert(dt["averageRating"] == 0)

def test_max_value_ratings():
    import sys
    rating_db.clear()
    add_rating_to_average(0, sys.maxsize)
    add_rating_to_average(0, sys.maxsize)
    correct_avg = 9223372036854775808
    dt = get_average_rating(product_id=0)
    assert(int(dt["averageRating"]) == correct_avg)

def test_min_value_ratings():
    import sys
    rating_db.clear()
    min_size = -sys.maxsize - 1
    add_rating_to_average(0, min_size)
    add_rating_to_average(0, min_size)
    correct_avg = -9223372036854775808
    dt = get_average_rating(product_id=0)
    assert(int(dt["averageRating"]) == correct_avg)

def test_db_count():
    rating_db.clear()
    add_rating_to_average(0, 2)
    add_rating_to_average(1, 2)
    rating = get_average_rating(product_id=0)
    assert(rating_db[0]["count"] == 1)
    assert(rating_db[1]["count"] == 1)

def test_insert_float_ratings():
    rating_db.clear()
    add_rating_to_average(0, 1.5)
    add_rating_to_average(0, 1.5)
    rating = get_average_rating(product_id=0)
    # consider rounding
    assert(rating["averageRating"] == 1.5)

def test_insert_float_int():
    rating_db.clear()
    add_rating_to_average(0, 2)
    add_rating_to_average(0, 1.5)
    rating = get_average_rating(product_id=0)
    # expeect rounding up
    assert(rating["averageRating"] == 1.8)
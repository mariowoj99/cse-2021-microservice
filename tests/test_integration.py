import pytest
from rating.rating import app

def test_get_rating():
    client = app.test_client()
    response = client.get("/getRating")
    assert response.status_code == 200
    assert response.json["averageRating"] is None

def test_add_rating():
    client = app.test_client()
    response = client.post(
            "/addRating",
            query_string = dict(
                product_id=0,
                rating=1
            )
    )
    assert response.status_code == 200
    response = client.get(
        "/getRating",
        query_string = dict(
            product_id=0
        )
    )
    assert response.status_code == 200
    assert response.json["averageRating"] == 1

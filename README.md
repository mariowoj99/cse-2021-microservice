# Product Rating Service

This is the source code for the assignment in the Continuous Software Engineering 2021/2022 at TU Berlin.

We are implementing a microservice which will run in the microservice demo [Online Botique](https://github.com/GoogleCloudPlatform/microservices-demo) and introduce a Rating service for the products in the shop.

## Deployment in Kubernetes

The Product Rating service extends the Online Botique microservice architecture from Google.
Therefore, the standard architecture provided by Google is a requirement for our service.

First, deploy the standard architecture:

    kubectl apply -f kubernetes-manifests.yaml
    
Since we needed to alter the frontend to expose our functionality, we provide an [own image](https://hub.docker.com/repository/docker/mariowoj99/frontend) with the necessary changes from us.
The repository with our changes to the frontend can be found [here](https://gitlab.com/mariowoj99/microservices-demo/-/tree/test-frontend).

Deploy the altered frontend:

    kubectl apply -f frontend.yaml

Lastly, deploy the Product Rating Service:

    kubectl apply -f ratingservice.yaml

The image used for the deployment can be found in [this repository](https://hub.docker.com/repository/docker/mariowoj99/ratingservice)


### Docker

If you want to build the image and run the service youself run:

    docker build . -t ratingservice

    docker run -p 5000:5000 ratingservice

## Development

### Requirements

#### Install `pipenv` with [pip](https://pypi.org/project/pip/):

    pip install pipenv

#### Install required packages:
    
    pipenv install
    
This will install all required packages into the pipenv virtual environment.

#### Create shell for virtual environment:
    
    pipenv shell

This will spin up the virtual environment for you, in which the application will run in.

#### (Inside shell) Start application:
    flask run

#### To leave the virtual environment:
    exit

#### If you want to remove the virtual environment again:
    pipenv --rm

The application will run under http://localhost:5000

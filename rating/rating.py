from flask import Flask, request
app = Flask(__name__)

rating_db = {}


@app.route("/getRating", methods = ["GET"])
def get_rating():
    product_id = request.args.get("product_id")
    return get_average_rating(product_id)

@app.route("/addRating", methods = ["POST"])
def add_rating():
    product_id = request.args.get("product_id")
    rating = float(request.args.get("rating"))

    return add_rating_to_average(product_id, rating)



def get_average_rating(product_id):

    if product_id in rating_db:
        avg = rating_db[product_id]["avg"]
        return {"product_id":product_id, "averageRating":round(avg,1)}
    
    return {"product_id":product_id, "averageRating":None}

def add_rating_to_average(product_id, rating):
    
    if product_id in rating_db:
        avg = rating_db[product_id]["avg"]
        count = rating_db[product_id]["count"]
    else:
        avg = 0
        count = 0
    
    new_avg = ((avg * count) + rating) / (count + 1)
    
    rating_db[product_id] = {"avg": new_avg, "count": count + 1}

    return {"product_id":product_id, "averageRating":round(new_avg,1)}

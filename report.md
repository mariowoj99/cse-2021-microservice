---
author:
    - Joshua Bauer
    - Jan-Niklas Bluhm
    - Mario Wojciechowski
title: "Microservice Implementation Report"
date: 20.02.2022
---

# Contribution
https://gitlab.com/mariowoj99/cse-2021-microservice

We collaborated on all parts of specification, infrastructure, implementation and testing and consider the contributions of all three team members to be equal.  


# Development Approach

We used Kanban as our project management methodology. For the initial brainstorming phase of the project, where
we decided upon which feature to implement, all team members were involved. From the beginning, we framed
every aspect of this process as a story. Since the assignment requirements focused clearly on providing a 
simple implementation, we focused our brainstorming on choosing a feature that was simple to implement
and didn't require integration at many points of the microservice demo.

## Product rating service 
Ultimately, we settled on building a product rating service. Conceptually, the service is very simple.
Users should be able to view for each product a rating defined as an Integer from 1 to 5. If, for their current session, they have not yet rated a product, they should be able to rate the product and view the updated product rating average. From these requirements, we implemented two API endpoints. The first endpoint allows for 
a GET request to fetch the current rating and the second endpoint makes a POST request to update the displayed
average rating by adding to it the user-supplied rating and recalculating the average with the new number of ratings.

To this end, we implemented two Python functions that are being wrapped by Flask API decorators. This decoupling
allows for easy implementation changes without changing the API paths and HTTP routing.

For persistent storage, in our minimum viable implementation, we decided to go with an in-memory key-value store
in the form of a Python dictionary. This implementation has several drawbacks.
Firstly, assuming an eventual horizontal scaling of the service, there is currently no mechanism to aggregate
in-memory stored average reviews by product id to correctly display the actual rating average.
This could be amended in three ways. The first option would be to introduce another layer of indirection and 
add functionality to the rating microservice for distributed consensus. The second option would be to
provision a new microservice that serves as an interface for a centralized storage (e.g. a Redis process) 
which the rating microservice could then read from and write to. The third option would be to add a type of load balancer which could shard requests by product id to ensure that data correctness is enforced.


All of these options have advantages and drawbacks. The first option would be algorithmically more complicated, while enjoying the benefits of distributed storage and being more architecturally fitting with the rest of the system. The second option would be significantly easier to implement, however, seeing as the possible rating storage service could face the exact same horizontal scaling problem as the rating service right now, this would only move the problem another layer away. The third option seems to be the most promising one, as this both distributes the workload and allows seamless scaling, however it would also introduce more components into the system and increase message load across the system.

Another deficiency of the current implementation is the fact that because the microservice demo does not have
the concept of user authentication, it is possible to manipulate user reviews since they are currently session-based. There are two options to amend this problem. The first option would be to add user authentication 
and a complete registration and login flow. This is complicated and would require significant changes to all 
parts of the application. The other option would be to move the rating user interface away from the product page to either the checkout service or to implement the user interface for the product rating via a one-time link in the order confirmation email, such that it would be guaranteed that only users who have actually bought an item are able to make a review for it.

# Technology

We decided to use Python as the implementation language for our new rating microservice.
We chose Python because of its high development speed, comprehensive standard library and because all team 
members are proficient with it. We chose Flask as our web-framework due to it making minimal assumptions
about other system components and its lightweight design. 
Flask, in comparison with the other major Python web framework Django, is less opinionated and therefore
allows for easier integration into a polyglot project.
We chose REST over HTTP as our communication technology, due to ease of implementation for the MVP and easy human-readable debugging possibilites. In further iterations of this project beyond the MVP, we would strongly consider either migrating to gRPC for better performance or to use asynchronous messaging via a message queue to add scalability and more elasticity with regard to bursty traffic to the rating service.

For our continuous integration,  we went with Gitlab actions due to the seamless integration into
Gitlab itself and because Gitlab courteously provides free CI/CD resources on their servers. 


# Quality

For the required feature focusing on quality, we decided to write several unit and integration tests to ensure the correctness of our implementation. We used the tool coverage.py to quantify the level of functional correctness of our code. We achieved
100\% code coverage. 
Additionally, we set up a CI/CD pipeline which runs our tests after every commit, to allow for continuous quality assurance.

Moreover, our CI/CD pipeline will build and push the latest docker image to our docker hub repository and also deploy it to our GCP cluster, resulting in an uncomplicated deployment phase.



FROM python:3.9-slim-buster

ENV FLASK_APP=wsgi.py

WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

EXPOSE 5000

WORKDIR /app/rating

CMD ["gunicorn","--bind","0.0.0.0:5000","wsgi:app"]
